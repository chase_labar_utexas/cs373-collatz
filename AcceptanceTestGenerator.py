known_lengths = {1:1}
known_set = set(known_lengths.keys())


def get_cycle_length(x):
    x_base = x
    cycle = 0
    while x not in known_set:
        if x % 2 == 0:
            x //= 2
            cycle += 1
        else:
            x = x + (x >> 1) + 1
            cycle += 2
    cycle += known_lengths[x]
    return cycle


def test_range(i, j):
    max_cycle = 0
    for x in range(i, j+1):
        max_cycle = max(max_cycle, get_cycle_length(x))
    print(str(i) + " " + str(j)) #+ " " + str(max_cycle))


for x in range(1, 55 + 1):
    for y in range(1, 4 + 1):
        test_range(x * 1003, (x * 1012) + y * 2056)